package MisUtilidades;

import java.util.Scanner;

import Api.ColaTDA;
import Api.HeapTDA;
import Api.PilaTDA;

public class MetodosEntradaSalida {
    public static void printHeap(HeapTDA heap){
        int size = heap.getHeapSize();
        for(int i=1 ; i<= size ; i++) {
            System.out.println(heap.findMax());
            heap.deleteMax();
        }
    }

    public static void cargarPilaEnteros(PilaTDA p) {
        try (Scanner teclado = new Scanner(System.in)) {
            int dato;
            System.out.print("Ingrese Dato (ingrese 0 para parar): ");
            dato = teclado.nextInt();
            while (dato != 0) {
                p.apilar(dato);
                System.out.print("Ingrese Dato (ingrese 0 para parar): ");
                dato = teclado.nextInt();
            }
        }
    }
    public static void mostrarPilaEnteros(PilaTDA p) {
        while (!p.pilaVacia()) {
            System.out.print(p.tope() + " - ");
            p.desapilar();
        }
        System.out.println();
    }

    public static void cargarColaEnteros(ColaTDA c){
        Scanner teclado = new Scanner(System.in);
        int dato;
        System.out.println("Ingrese Dato (ingrese 0 para parar): ");
        dato = teclado.nextInt();
        while (dato != 0){
            c.acolar(dato);
            System.out.print("Ingrese Dato (ingrese 0 para parar): ");
            dato = teclado.nextInt();
        }
        //teclado.close();
    }
    public static void mostrarColaEnteros(ColaTDA c){
        while (!c.colaVacia()){
            System.out.print(c.primero() + " - ");
            c.desacolar();
        }
        System.out.println();
    }

}
