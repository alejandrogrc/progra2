package MisUtilidades;

import Api.ColaPrioridadTDA;
import Api.ColaTDA;
import Api.PilaTDA;
import MisImplementaciones.Estaticas.Cola;
import MisImplementaciones.Estaticas.Pila;

public class Metodos {
    public void pasarPilaCola(ColaTDA dest, PilaTDA origen) {
        while(!origen.pilaVacia()) {
            dest.acolar(origen.tope());
            origen.desapilar();
        }
    }
    public void pasarPilaPila(PilaTDA dest, PilaTDA origen) {
        while (!origen.pilaVacia()) {
            dest.apilar(origen.tope());
            origen.desapilar();
        }
    }
    public void copiarPilaPila(PilaTDA dest, PilaTDA origen) {
        PilaTDA aux;
        aux = new Pila();
        aux.inicializarPila();
        pasarPilaPila(aux, origen);
        while(!aux.pilaVacia()) {
            origen.apilar(aux.tope());
            dest.apilar(aux.tope());
            aux.desapilar();
        }
    }

    public void copiarColaCola(ColaTDA dest, ColaTDA origen){
        ColaTDA colaAux = new Cola();
        colaAux.inicializarCola();
        pasarColaCola(colaAux, origen);
        while(!colaAux.colaVacia()){
            origen.acolar(colaAux.primero());
            dest.acolar(colaAux.primero());
            colaAux.desacolar();
        }
    }

    public void pasarColaCola(ColaTDA dest, ColaTDA origen) {
        while (!origen.colaVacia()) {
            dest.acolar(origen.primero());
            origen.desacolar();
        }
    }
    public void invertirPila(PilaTDA origen) {
        PilaTDA aux = new Pila();
        aux.inicializarPila();
        pasarPilaPila(aux,origen);
        copiarPilaPila(origen,aux);
    }
    public int contarPila(PilaTDA origen){
        int i = 0;
        while (!origen.pilaVacia()){
            i++;
            origen.desapilar();
        }
        return i;
    }
    public int sumarPila(PilaTDA origen){
        int i = 0;
        while (!origen.pilaVacia()){
            i = origen.tope() + i;
            origen.desapilar();
        }
        return i;
    }

    public void invertirCola(ColaTDA origen){
        PilaTDA aux = new Pila();
        aux.inicializarPila();
        while (!origen.colaVacia()){
            aux.apilar(origen.primero());
            origen.desacolar();
        }
        while (!aux.pilaVacia()){
            origen.acolar(aux.tope());
            aux.desapilar();
        }
    }

    public void invertirColaSinPila(ColaTDA origen){
        int [] vector = new int[100];
        int i = 1;
        while (!origen.colaVacia()){
            vector[i] = origen.primero();
            origen.desacolar();
            i++;
        }
        while (i != 0){
            origen.acolar(vector[i]);
            i--;
        }
    }

    public boolean compararFinalCola(ColaTDA c1, ColaTDA c2){
        int valorC1=0, valorC2=0;

        while (!c1.colaVacia()){
            valorC1 = c1.primero();
            c1.desacolar();
        }
        while (!c2.colaVacia()){
            valorC2 = c2.primero();
            c2.desacolar();
        }
        return valorC1==valorC2;
    }

    public boolean esColaCapicua(ColaTDA c1){
        PilaTDA paux = new Pila();
        paux.inicializarPila();

        ColaTDA caux = new Cola();
        caux.inicializarCola();


        while(!c1.colaVacia()){
            paux.apilar(c1.primero());
            caux.acolar(c1.primero());
            c1.desacolar();
        }

        while(!paux.pilaVacia()){
            if(paux.tope() != caux.primero()){
                return false;
            }
            paux.desapilar();
            caux.desacolar();
        }
        return true;
    }

    public boolean esColaInversa(ColaTDA c1, ColaTDA c2){
        //Falta hacer la verifacición si las colas tienen el mismo tamaño
        PilaTDA paux = new Pila();
        paux.inicializarPila();

        while(!c1.colaVacia()){
            paux.apilar(c1.primero());
            c1.desacolar();
        }
        while(!c2.colaVacia()){
            if(paux.tope() != c2.primero())
                return false;
            c2.desacolar();
            paux.desapilar();
        }
        return true;
    }

    public ColaTDA spiegel(ColaTDA u) {
        ColaTDA q = new Cola();
        q.inicializarCola();
        while (!u.colaVacia()) {
            int x = u.primero();
            u.desacolar();
            q = spiegel(u);
            q.acolar(x);
        }
        return q;
    }
/*
    public void Sacar(int x ) {
        if( c != null) {
            if ( c.info == x ) {
                c = c.sig ;
            } else {
                Nodo aux = c ;
                while ( aux.sig != null && aux.sig.info != x )
                    aux = aux.sig ;
                if ( aux.sig != null )
                    aux.sig = aux.sig.sig ;
            }
        }
    }

    public void generar_lista(Nodo inicio_lista, int x){
        int sumataria=0;


        Nodo aux = inicio_lista;
        while (( aux != null )) {

            if(aux.sig.info == x){
                nuevo = new Nodo();
                sumatoria++;
                nuevo.info = 0;
                nuevo.sig = aux;
            }
            aux = aux . sig ;
        }
        // si es el primero
        if (aux.info == x && aux.sig == null) {
            nuevo = new Nodo();
            sumatoria++;
            nuevo.info = 0;
            nuevo = aux.sig;
            nuevo.sig = null;
        }
        return ( aux != n u l l ) ;

    }
    public Nodo invert (Nodo inicio) {
        Nodo rev = new Nodo();
        Nodo aux = new Nodo();
        rev = null
        aux = inicio
        while (aux ! = null) {
            nuevo = new Nodo();
            nuevo.info = aux.info;
            nuevo.sig = rev;
            rev = nuevo;
            aux = aux.sig;
        }
        return rev;
    }

    public int Separar(ColaTDA bloques, ColaTDA mensaje,ColaTDA relleno){
        PilaTDA paux = new Pila();
        paux.inicializarPila();

        PilaTDA paux2 = new Pila();
        paux2.inicializarPila();


        int multiplo = 0;
        int mensajeRetorno = 0;

        while(!bloques.colaVacia()){
            multiplo++;
            paux.apilar(bloques.primero());
            paux2.apilar(bloques.primero());

            bloques.desacolar();
        }

        if(multiplo % 8 == 0){
            multiplo = multiplo/8;
            while (multiplo != 0){

                if(multiplo == 1){
                    while (!paux2.pilaVacia()){
                        if()
                        paux2.desapilar();
                    }
                }
                mensaje.acolar(paux2.tope());
                paux2.desapilar();
            }
        }
        else
            mensajeRetorno = 1;

        while(!paux.pilaVacia()){
            bloques.acolar(paux.tope());
            paux.desapilar();
        }

        return mensajeRetorno;
    }
*/

    /*
        La estrategia que se va a utilizar es la siguiente:
            1. Tomo de entrada las pilas.
            2. Creo una pila Auxiliar,2 flag booleanos, y un entero auxiliar.
            3. Recorro cualquiera de las 2 pilas (de aqui, llamada p1), en cada iteración saco el tope de la que estoy recorriendo. Clono la otra pila (p2) en la pila auxiliar. (Debajo se detallan los metodos usados para copiar pila)
            4. Recorro la pila auxiliar, sacando el tope y comparandola con el tope de p1.
            5. Si encuentro el valor de p1 en p2, fijo el flag auxiliar en true. Si no lo encuentra, sigo en false.
            6. Desapilo la pila auxiliar para seguir recorriendola.
            6. Cuando termino de recorrer, chequeo el flag auxiliar. Si el numero no estaba en la pila. Cambio el flag a devolver.
            7. Despilo p1 y repito hastan que quede vacia.
            8. Devuelvo booleano


     */
    public boolean Equiv(PilaTDA p1, PilaTDA p2){
        PilaTDA p2Aux = new Pila();

        boolean flagEquivAux = false;
        boolean flagEquiv = true;
        int aux = 0;

        p2Aux.inicializarPila();

        while(!p1.pilaVacia()){
            copiarPilaPila(p2Aux,p2);
            aux = p1.tope();
            while(!p2Aux.pilaVacia()){
                if(aux == p2Aux.tope()){
                    flagEquivAux = true;
                }
                p2Aux.desapilar();
            }
            if(!flagEquivAux){
                flagEquiv = false;
            }
            p1.desapilar();
        }
        return flagEquiv;
    }


    /*
    public void pasarPilaPila(PilaTDA dest, PilaTDA origen) {
        while (!origen.pilaVacia()) {
            dest.apilar(origen.tope());
            origen.desapilar();
        }
    }
    public void copiarPilaPila(PilaTDA dest, PilaTDA origen) {
        PilaTDA aux;
        aux = new Pila();
        aux.inicializarPila();
        pasarPilaPila(aux, origen);
        while(!aux.pilaVacia()) {
            origen.apilar(aux.tope());
            dest.apilar(aux.tope());
            aux.desapilar();
        }
    }
     */

    /*
        El costo del metodo pasarPilaPila es lineal.
        El costo del metodo copiarPilaPila es lineal.
        El costo del metodo Equiv es cuadratico ya que recorro 1 pila por cada elemento de la otra pila.

        Se suman 1 costo lineal( pasarPilaPila ) + 2 costo lineal (copiarPilaPila + pasarPilaPila) al recorrer con un while se vuelve cuadratico + el otro while, que es otro cuadratico.
        El costo total es cuadratico.

     */

    public int obtenerMayorPrimerosTres(ColaTDA c1){
        ColaTDA aux = new Cola();
        aux.inicializarCola();

        int [] array = new int[3];
        int mayor = 0;
        copiarColaCola(aux,c1);
        for(int i=0;i<3;i++){
            array[i] = aux.primero();
            aux.desacolar();
        }
        for(int i=0;i<3;i++){
            if(array[i]>mayor)
                mayor=array[i];
        }
        return mayor;

    }

    public int contarCola(ColaTDA c1){
        ColaTDA aux = new Cola();
        aux.inicializarCola();

        copiarColaCola(aux,c1);

        int tamano = 0;

        while(!aux.colaVacia()){
            tamano++;
            aux.desacolar();
        }
        return tamano;
    }

    public void parcial(ColaTDA cc, ColaTDA ccc){
        int tamano = 0;

        tamano = contarCola(cc);
        for(int i=0;i<tamano-2;i++){
            ccc.acolar(obtenerMayorPrimerosTres(cc));
            cc.desacolar();
        }
    }
}
