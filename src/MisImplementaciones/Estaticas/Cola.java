package MisImplementaciones.Estaticas;

import Api.ColaTDA;

//Implementación acotada a 100 elementos. Almacenamiento circular

public class Cola implements ColaTDA {
    int [] vector;
    int entrada,salida;

    @Override
    public void inicializarCola() {
        vector = new int[100];
        entrada = salida = 0;
    }

    @Override
    public void acolar(int x) {
        vector[entrada] = x;
        entrada++;
        if (entrada>100)
            entrada=0;
    }

    @Override
    public void desacolar() {
        salida++;
        if (salida>100)
            salida=0;
    }

    @Override
    public int primero() {
        return vector[salida];
    }

    @Override
    public boolean colaVacia() {
        return entrada==salida;
    }
}
