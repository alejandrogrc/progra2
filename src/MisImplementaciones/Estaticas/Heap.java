package MisImplementaciones.Estaticas;

import Api.HeapTDA;
// en esta implementación el Heap arranca en la posición 1 del array para facilitar codigo
public class Heap implements HeapTDA {

    int [] arrayHeap;
    int currentSize; // tamaño del heap actual

    /*
     *   Metodo initializeHeap: Tiene un costo constante O(1)
     *   Explicación: inicializo Heap
     * */
    @Override
    public void initializeHeap() {
        arrayHeap = new int[100];
        currentSize=0;
    }


    /*
    *   Metodo findMax: Tiene un costo constante O(1)
    *   Explicación: Devuelve el valor mayor del arbol (se encuentra en la raiz del arbol)
    * */
    @Override
    public int findMax() {
        return arrayHeap[1];
    }

    /*
     *   Metodo deleteMax: Tiene un costo logaritmico en el peor caso O(log n). Ver siftDown para detalle.
     *   Explicación: Borro el nodo mas grande (raiz). Lo intercambio con el ultimo nodo y le hago un siftDown para reubicarlo en el Heap
     * */
    @Override
    public void deleteMax() {
        if (isEmpty())
            throw new ArrayIndexOutOfBoundsException("Heap vacio");
        arrayHeap[1] = arrayHeap[currentSize];
        currentSize--;
        siftDown(1);
    }

    /*
     *   Metodo insertNode: Tiene un costo logaritmico en el peor caso O(log n). Ver Percolate para detalle.
     *   Explicación: Borro el nodo mas grande (raiz). Lo intercambio con el ultimo nodo y le hago un siftDown para reubicarlo en el Heap
     * */

    @Override
    public void insertNode(int element) {
        if(isFull())
            throw new ArrayIndexOutOfBoundsException("Heap lleno");
        currentSize++;
        arrayHeap[currentSize]=element;
        percolate(currentSize);
    }

    /*
     *   Metodo isEmpty: Tiene un costo constante O(1).
     *   Explicación: Veo si esta vacio el Heap
     * */
    @Override
    public boolean isEmpty() {
        return currentSize==0;
    }

    /*
     *   Metodo makeHeap(o Heapify): Tiene un costo logaritmico en el peor caso O(log n). Ya que este metodo se recorre una vez por cantidad de Nodos sobre 2. Ver siftDown para detalle.
     *   Explicación: A partir de un array desordenado creo un arbol Heap, respetando su propiedad de Heap.
     * */
    @Override
    public void makeHeap(int[] array, HeapTDA newHeap) {
        arrayHeap = array;
        currentSize = array.length - 1;

        for(int i=currentSize/2; i > 0; i--)
            siftDown(i);
    }

    /*
     *   Metodo isFull: Tiene un costo constante O(1).
     *   Explicación: Veo si esta lleno el Heap
     * */
    @Override
    public boolean isFull() {
        return currentSize==arrayHeap.length;
    }

    /*
     *   Metodo isFull: Tiene un costo constante O(1).
     *   Explicación: Veo el tamaño de los nodos del Heap
     * */
    @Override
    public int getHeapSize() {
        return currentSize;
    }

    /*
     *   Metodo HeapSort: Tiene un costo n*logaritmico en el peor caso O(nlog n). En el peor de los casos recorrer el Heap 1 vez por nodo y lo ordena(Loritmico)
     *   Explicación: A partir de un Nodo se restablece la propiedad de Heap, subiendo dicho nodo por el arbol hasta que llegue a su posición correspondiente. Luego se ordena
     * */
    @Override
    public void heapSort(int[] array, HeapTDA newHeap) {
        int aux;
        makeHeap(array,newHeap);
        for(int i=currentSize; i>2 ; i--){
            aux = arrayHeap[i];
            arrayHeap[i]=arrayHeap[1];
            arrayHeap[1]=aux;
            siftDown(i);
        }

    }


    /*
     *   Metodo interno percolate: Tiene un costo logaritmico en el peor caso O(log n). En el while tiene costos logaritmico en el peor caso que sería insertar un Nodo que sea raiz, se movería hasta la raiz del Heap.
     *   Explicación: A partir de un Nodo se restablece la propiedad de Heap, subiendo dicho nodo por el arbol hasta que llegue a su posición correspondiente
     * */
    private void percolate(int i){
        int k=i,j=-1,aux;

        while(j!=k){
            j = k;
            if (j > 1 && arrayHeap[j/2] < arrayHeap[k]){
                k=j/2;
                aux = arrayHeap[k];
                arrayHeap[k]=arrayHeap[j];
                arrayHeap[j]=aux;
            }
        }

    }
    /*
     *   Metodo interno siftDown: Tiene un costo logaritmico en el peor caso O(log n). En el while tiene costo logaritmico en el peor caso que sería tener un Nodo con la prioridad mas baja sobre el nivel Raiz.
     *   Explicación: A partir de un Nodo se restablece la propiedad de Heap, bajando dicho nodo por el arbol hasta que llegue a su posición correspondiente
     * */
    private void siftDown(int i){
        int k=i,j=-1,aux;

        while(j!=k){
            j=k;
            if( 2*j <= currentSize && arrayHeap[2*j] > arrayHeap[k])
                k=2*j;
           if (2 * j + 1 < currentSize) {
               if (2 * j < currentSize && arrayHeap[2 * j + 1] > arrayHeap[k])
                    k = 2 * j + 1;
           }

            aux = arrayHeap[k];
            arrayHeap[k]=arrayHeap[j];
            arrayHeap[j]=aux;

        }
    }
}
