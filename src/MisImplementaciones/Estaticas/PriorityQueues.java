package MisImplementaciones.Estaticas;

import Api.HeapTDA;
import Api.PriorityQueuesTDA;

public class PriorityQueues implements PriorityQueuesTDA {

    int [] arrayPriority;
    int [] arrayData;
    int currentSize; // tamaño del heap actual


    @Override
    public void initializePriorityQueues() {
        arrayPriority = new int[100];
        arrayData = new int[100];
        currentSize=0;
    }

    @Override
    public int findMax() {
        return 0;
    }

    @Override
    public void deleteMax() {

    }

    @Override
    public void insertNode(int element, int priority) {
        if(isFull())
            throw new ArrayIndexOutOfBoundsException("Priority Queue lleno");
        currentSize++;
        arrayPriority[currentSize] = priority;
        arrayData[currentSize] = element;
        percolate(currentSize);
    }



    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public void makeHeap(int[] array, HeapTDA newHeap) {

    }

    @Override
    public boolean isFull() {
        return currentSize==100;
    }

    @Override
    public void printHeap() {
        for(int i=1 ; i<currentSize+1 ; i++)
            System.out.println(arrayPriority[i] + "-" + arrayData[i]);
    }


    /*
    *   Metodo Percolate: Tiene costo
    *
    * */
    private void percolate(int i){
        int k=i,j=-1,aux;

        while(j!=k){
            j = k;
            if (j > 1 && arrayPriority[j/2] < arrayPriority[k]){
                k=j/2;
                aux = arrayPriority[k];
                arrayPriority[k]=arrayPriority[j];
                arrayPriority[j]=aux;
                aux = arrayData[k];
                arrayData[k]=arrayData[j];
                arrayData[j]=aux;
            }
        }
    }
    private void siftDown(int i){
        int k=i,j=-1,aux;

        while(j!=k){
            j=k;
            if( 2*j <= currentSize && arrayPriority[2*j] > arrayPriority[k])
                k=2*j;
            if( 2*j < currentSize && arrayPriority[2*j+1] > arrayPriority[k])
                k=2*j+1;
            aux = arrayPriority[k];
            arrayPriority[k]=arrayPriority[j];
            arrayPriority[j]=aux;
            aux = arrayData[k];
            arrayData[k]=arrayData[j];
            arrayData[j]=aux;

        }
    }
}
