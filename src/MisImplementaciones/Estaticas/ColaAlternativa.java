package MisImplementaciones.Estaticas;

import Api.ColaTDAAlternativa;
import Api.PilaTDA;


// Esta implementación es teniendo en cuenta que la cola NO es circular, en cuyo caso habria que establecer un valor que haga las veces de "vacio" para poder emular la cola circular
public class ColaAlternativa implements ColaTDAAlternativa{
    PilaTDA p1 = new Pila();
    PilaTDA p2 = new Pila();


    @Override
    public void inicializarCola() {
        p1.inicializarPila();
        p2.inicializarPila();
    }

    @Override
    public void acolar(int x) {
        p1.apilar(x);
    }

    @Override
    public void desacolar() {
        while (!p1.pilaVacia()){ // Vuelco la pila 1 a la 2 para invertirla
            p2.apilar(p1.tope());
            p1.desapilar();
        }
        p1.desapilar(); // Saco el tope de la lista que seria el primer valor de la cola
        while (!p2.pilaVacia()){ // Vuelvo a dejar la "cola" como estaba
            p1.apilar(p2.tope());
            p2.desapilar();
        }
    }

    @Override
    public int primero() {
        int numero = 0; // Es el numero que va a ser primero
        while (!p1.pilaVacia()){ // Vuelco la pila 1 a la 2 para invertirla
            p2.apilar(p1.tope());
            p1.desapilar();
        }
        numero = p2.tope(); // Saco el ultimo numero que seria el primero de la cola circular

        while (!p2.pilaVacia()){ // Vuelvo a dejar la "cola" como estaba
            p1.apilar(p2.tope());
            p2.desapilar();
        }

        return numero; // Devuelvo el primero
    }

    @Override
    public boolean colaVacia() {
        return p1.pilaVacia();
    }
}
