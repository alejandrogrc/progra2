package MisImplementaciones.Dinamicas;

import Api.HeapTDA;

public class Heap implements HeapTDA {
    class Node{
        int value;
        int actualNode;
        Node next;
    }
    Node root;
    int currentSize;

    @Override
    public void initializeHeap() {
        root=null;
        currentSize=0;
    }

    @Override
    public int findMax() {
        return 0;
    }

    @Override
    public void deleteMax() {

    }

    @Override
    public void insertNode(int element) {
        Node newNode = new Node();
        newNode.value = element;
        newNode.next = root;
        root = newNode;
        currentSize++;
        newNode.actualNode = currentSize;
    }
/*
    private void percolate(Node nodeNumber){
        int k=nodeNumber.actualNode, j=-1;
        Node aux = new Node();

        while(j!=k){
            j = k;
            if(j > 1 && nodeNumber.value )

        }
    }

    private void percolate1(int i){
        int k=i,j=-1,aux;

        while(j!=k){
            j = k;
            if (j > 1 && arrayHeap[j/2] < arrayHeap[k]){
                k=j/2;
                aux = arrayHeap[k];
                arrayHeap[k]=arrayHeap[j];
                arrayHeap[j]=aux;
            }
        }
*/
    @Override
    public boolean isEmpty() {
        return currentSize==0;
    }

    @Override
    public void makeHeap(int[] array, HeapTDA newHeap) {

    }

    @Override
    public boolean isFull() {
        return false;
    }

    @Override
    public int getHeapSize() {
        return 0;
    }

    @Override
    public void heapSort(int[] array, HeapTDA newHeap) {

    }

    @Override
    public void printHeap() {

    }

}
