package Api;

public interface ColaTDAAlternativa {
    /** Inicializo cola. Precondición: ninguna **/
    void inicializarCola();
    /** Agrega elemento a la cola (FIFO). Precondición: la cola debe estar inicializada **/
    void acolar(int x);
    /** Saco ultimo elemento ingresado de la cola (FIFO). Precondición: la cola no debe estar vacia **/
    void desacolar();
    /** Obtengo el primer elemento de la cola (FIFO). Precondición: la cola no debe estar vacia **/
    int primero();
    /** Verifico si la cola se encuentra vacia. Precondición: la cola debe estar inicializada **/
    boolean colaVacia();
}
