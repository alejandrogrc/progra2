package Api;

public interface ColaPrioridadTDA {
    /** Inicializo cola con prioridad. Precondición: ninguna **/
    void inicializarCola();
    /** Agrega elemento, con su prioridad a la cola. Precondición: la cola debe estar inicializada **/
    void acolarPrioridad(int x, int p);
    /** Saco el elemento de mayor prioridad de la cola. Precondición: la cola no debe estar vacia **/
    void desacolar();
    /** Obtengo el elemento de mayor prioridad de la cola. Precondición: la cola no debe estar vacia **/
    int primero();
    /** Obtengo la prioridad del dato de mayor prioridad de la cola. Precondición: la cola no debe estar vacia **/
    int prioridad();
    /** Verifico si la cola se encuentra vacia. Precondición: la cola debe estar inicializada **/
    boolean colaVacia();
}
