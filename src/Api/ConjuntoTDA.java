package Api;

public interface ConjuntoTDA {
    /** Inicializo conjunto. Precondición: ninguna **/
    void inicializarConjunto();
    /** Agrego un elemento al conjunto. Precondición: el conjunto debe estar inicializado y no debe existir el numero a agregar en el conjunto **/
    void agregar(int x);
    /** Elimino elemento del conjunto. Precondición: el elemento debe pertenecer al conjunto **/
    void sacar(int x);
    /** Obtengo un valor cualquiera del conjunto. Precondición: No debe estar vacio el conjunto **/
    int obtener();
    /** Verifico si el conjunto esta vacio. Precondición: debe estar inicializado el conjunto **/
    boolean conjuntoVacio();
    /** Verifico si un elemento perternece o no al conjunto. Precondición: el conjunto debe estar incializado **/
    boolean pertenece(int x);
}
