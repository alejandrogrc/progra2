package Api;

public interface DiccionarioMultipleTDA {
    /** Inicializa el diccionario simple. Precondición: ninguna**/
    void inicializarDiccionarioSimple();
    /** Agrego un elemento a una clave dada. Precondición: el diccionario debe estar inicializado y la clave no debe existir**/
    void agregar(int x, int c);
    /** Elimino clave dada. Precondición: la clave debe existir**/
    void eliminar(int c);
    /** Obtengo el valor asociado a una clave. Precondición: la clave debe existir**/
    int obtener(int c);
    /** Devuelvo el conjunto de claves del diccionario. Precondición: el diccionario debe estar inicializado**/
    ConjuntoTDA claves();
}
