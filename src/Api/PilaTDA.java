package Api;

public interface PilaTDA {
    /** Inicializo pila. Precondición: ninguna **/
    void inicializarPila();
    /** Agrega elemento a la pila (al final) LIFO. Precondición: la pila debe estar inicializada **/
    void apilar(int x);
    /** Saco ultimo elemento de la pila (LIFO). Precondición: la pila no debe estar vacia **/
    void desapilar();
    /** Obtengo el ultimo elemento de la pila (LIFO). Precondición: la pila no debe estar vacia **/
    int tope();
    /** Chequeo si la pila esta vacia. Precondición: la pila debe estar inicializada  **/
    boolean pilaVacia();
}
