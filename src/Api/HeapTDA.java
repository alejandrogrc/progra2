package Api;

public interface HeapTDA {
    void initializeHeap();
    int findMax();
    void deleteMax();
    void insertNode(int element);
    boolean isEmpty();
    void makeHeap(int [] array, HeapTDA newHeap);
    boolean isFull(); // Solo aplica en implementación estatica
    int getHeapSize();
    void heapSort(int [] array, HeapTDA newHeap);
}
