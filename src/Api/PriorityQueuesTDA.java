package Api;

public interface PriorityQueuesTDA {
    void initializePriorityQueues();
    int findMax();
    void deleteMax();
    void insertNode(int element, int priority);
    boolean isEmpty();
    void makeHeap(int [] array, HeapTDA newHeap);
    boolean isFull(); // Solo aplica en implementación estatica
    void printHeap(); // Solo pára probar, borrar!
}
