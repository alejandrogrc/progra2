package Aplicaciones;

import Api.ColaTDA;
import Api.HeapTDA;
import Api.PriorityQueuesTDA;
import MisImplementaciones.Estaticas.Cola;
import MisImplementaciones.Estaticas.Heap;
//import MisImplementaciones.Dinamicas.Heap;
import MisImplementaciones.Estaticas.Pila;
import Api.PilaTDA;
import MisImplementaciones.Estaticas.PriorityQueues;
import MisUtilidades.Metodos;
import MisUtilidades.MetodosEntradaSalida;

import java.util.Scanner;

import static MisUtilidades.MetodosEntradaSalida.cargarPilaEnteros;

public class TP1 {
    public static void main (String[] args){
//        PilaTDA p1 = new Pila();
//        PilaTDA p2 = new Pila();
//
//        ColaTDA c1 = new Cola();
//        ColaTDA c2 = new Cola();

        int [] array = new int[]{0,9,10,4,5,8,7,6,2,3,1,22,21,33,55,77,88,99,312,16,69};

        Metodos m = new Metodos();
        MetodosEntradaSalida n = new MetodosEntradaSalida();
//        int data;
//        int i;
//        p1.inicializarPila();
//        p2.inicializarPila();
//
//        c1.inicializarCola();
//        c2.inicializarCola();

        HeapTDA heap1 = new Heap();
        heap1.initializeHeap();
        HeapTDA heap2 = new Heap();
        heap2.initializeHeap();

//        PriorityQueuesTDA q1 = new PriorityQueues();
//        q1.initializePriorityQueues();
//        q1.insertNode(5,1);
//        q1.insertNode(6,2);
//        q1.insertNode(8,9);
//        q1.insertNode(9,5);
//        q1.printHeap();

        /*
        heap1.insertNode(5);
        heap1.insertNode(6);
        heap1.insertNode(10);
        heap1.insertNode(8);
        heap1.insertNode(7);
        heap1.insertNode(3);
        heap1.insertNode(4);
        heap1.insertNode(9);
        heap1.insertNode(2);
        heap1.insertNode(1);
        heap1.printHeap();
        System.out.println("----------------");
        heap1.deleteMax();
        heap1.printHeap();
        System.out.println("----------------");
        heap2.makeHeap(array,heap2);
        //heap2.printHeap();


         */
        System.out.println("----------------");
        //heap2.makeHeap(array,heap2);
        heap1.heapSort(array,heap1);
        n.printHeap(heap1);
        System.out.println("----------------");
        heap2.makeHeap(array,heap2);
        n.printHeap(heap2);
        //n.cargarColaEnteros(c1);
        //m.invertirColaSinPila(c1);
        //n.mostrarColaEnteros(c1);
        //n.cargarColaEnteros(c2);
        //m.invertirCola(c1);
        //if (m.esColaInversa(c1,c2))
        //    System.out.println("Es inversa");
        //else
        //   System.out.println("No es inversa");
        //c2 = m.spiegel(c1);
        //n.mostrarColaEnteros(c1);
        //n.cargarPilaEnteros(p1);
        //System.out.println("Es inversa");
        //n.cargarPilaEnteros(p2);

//        c1.acolar(1);
//        c1.acolar(2);
//        c1.acolar(-4);
//        c1.acolar(3);
//        c1.acolar(2);
//        c1.acolar(1);
//        c1.acolar(-3);
//        c1.acolar(0);
//        c1.acolar(1);
//        c1.acolar(-3);
//        m.parcial(c1,c2);
       // n.mostrarColaEnteros(c2);
        //System.out.println(m.obtenerMayorPrimerosTres(c1));
        //System.out.println(m.contarCola(c1));

    /*
        if(m.Equiv(p1,p2))
            System.out.println("Es equivalente");
        else
            System.out.println("No es equiv");

 */
        //m.copiarPilaPila(p2,p1);
        //m.invertirPila(p1);
        //n.mostrarPilaEnteros(p2);
        //n.mostrarPilaEnteros(p1);
        //System.out.println(m.contarPila(p1));

        //System.out.println(m.sumarPila(p1));

    }

}
